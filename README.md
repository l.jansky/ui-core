# PREJT #

Package for providing front end administration (mainly forms, menus, form components). All forms are defined by XML files. Form components are provided during initialization of this package (from prejt-components-material package, which provides material components)

Definition of form can look like this example:

```
<module>
	<endpoint fetch="/static-endpoints/menu.json" data="/menu/items" />
	<menu data="/menu/items" items=".">
		<menu-item label="./title" click="redirect;./module" />
	</menu>

	<layout module="$module" />
</module>
```

Its base module for all sections of administration, has menu with data loaded from endpoint and stored in /menu/items path in redux store (using data reducer)

There is also layout part, which is able to display another module inside (defined by module attribute). $module means it is module name taken from url.

For example if there is module manufacturers set in url, file manufacturers.xml is used for module showed inside layout.

```
<module>

	<endpoint fetch="/static-endpoints/manufacturers" data="/manufacturers" />

	<box label="Manufacturers">

		<grid data="/manufacturers" items=".">
			
			<col label="id">
				<string text="./id" />
			</col>

			<col label="name">
				<string text="./name" />
			</col>

			<col label="Country" collapsible="1">
				<string text="./country/name" />
			</col>

			<col>
				<button label="Detail" click="redirect;$module;./id" />
			</col>

		</grid>
	</box>

	<drawer visible="$id" module="forms/manufacturer" />

</module>
```