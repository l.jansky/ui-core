import { DataValue } from '../store';

export type EventContext = string[];

export type EventListener = (
	eventContext: EventContext,
	data: DataValue
) => void;

export type EventEmitter = {
	emit: (event: string, context: EventContext, data: DataValue) => void;
	on: (event: string, listener: EventListener) => void;
	removeListener: (event: string, listener: EventListener) => void;
};
