import React from 'react';
import { render, act } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';
import { EventsContextProvider } from './events.context';
import { eventEmitterFactory } from './event-emitter';
import { useEvents } from './events.hook';
import { EventEmitter } from './events.types';

const getWrapper = (eventEmitter: EventEmitter): React.FC => {
	return ({ children }) => (
		<EventsContextProvider eventEmitter={eventEmitter}>
			{children}
		</EventsContextProvider>
	);
};

describe('Events hook', () => {
	beforeEach(() => {
		jest.clearAllMocks();
	});

	const eventName = 'eventName';
	const data = { test: 1 };

	it('should return event data if event is called and subscribed in same context', () => {
		const eventCallback = jest.fn();
		const emitter = eventEmitterFactory()();
		renderHook(() => useEvents([eventName], ['root'], eventCallback), {
			wrapper: getWrapper(emitter)
		});

		act(() => {
			emitter.emit(eventName, ['root'], data);
		});

		expect(eventCallback).toHaveBeenCalledWith(data);
	});

	it('should ignore event if it is called in context which is not part of subscribed context', () => {
		const eventCallback = jest.fn();
		const emitter = eventEmitterFactory()();
		renderHook(
			() => useEvents([eventName], ['root', 'subContext1'], eventCallback),
			{
				wrapper: getWrapper(emitter)
			}
		);

		act(() => {
			emitter.emit(eventName, ['root', 'subContext2'], data);
		});

		act(() => {
			emitter.emit(eventName, ['root'], data);
		});

		expect(eventCallback).not.toHaveBeenCalled();
	});

	it('should return event data if event is called in sub-context of subscribed context', () => {
		const eventCallback = jest.fn();
		const emitter = eventEmitterFactory()();
		renderHook(() => useEvents([eventName], ['root'], eventCallback), {
			wrapper: getWrapper(emitter)
		});

		act(() => {
			emitter.emit(eventName, ['root', 'subContext'], data);
		});

		expect(eventCallback).toHaveBeenCalledWith(data);
	});

	it('should return event data from more different events', () => {
		const eventCallback = jest.fn();
		const emitter = eventEmitterFactory()();
		renderHook(() => useEvents(['event1', 'event2'], ['root'], eventCallback), {
			wrapper: getWrapper(emitter)
		});

		act(() => {
			emitter.emit('not-subscribed', ['root'], 'anything');
		});

		expect(eventCallback).not.toHaveBeenCalled();

		act(() => {
			emitter.emit('event1', ['root'], 'a');
		});

		expect(eventCallback).toHaveBeenLastCalledWith('a');

		act(() => {
			emitter.emit('event2', ['root'], 'b');
		});

		expect(eventCallback).toHaveBeenLastCalledWith('b');
	});

	describe('Tests inside components', () => {
		const eventContext: string[] = [];
		const eventName = 'test-event';

		const TestEventComponent: React.FC<{ listenTo: string[] }> = ({
			listenTo
		}) => {
			const [text, setText] = React.useState('');
			useEvents(listenTo, eventContext, data => {
				setText((text + data) as string);
			});
			return <div data-testid="test-text">{text}</div>;
		};

		const TestWrap: React.FC<{ emitter: EventEmitter }> = ({ emitter }) => {
			return (
				<EventsContextProvider eventEmitter={emitter}>
					<TestEventComponent listenTo={[eventName]} />
				</EventsContextProvider>
			);
		};

		it('should not unsubscribe listener if changed events array still includes that event', async () => {
			const eventEmitter = eventEmitterFactory()();
			const removeListenerSpy = jest.spyOn(eventEmitter, 'removeListener');

			const { rerender } = render(<TestWrap emitter={eventEmitter} />);
			await act(async () => {
				rerender(<TestWrap emitter={eventEmitter} />);
			});

			expect(removeListenerSpy).not.toHaveBeenCalled();
		});

		it('should run callback with actual data', async () => {
			const eventEmitter = eventEmitterFactory()();

			const { getByTestId } = render(<TestWrap emitter={eventEmitter} />);
			await act(async () => {
				eventEmitter.emit(eventName, eventContext, 'a');
			});

			expect(getByTestId('test-text').textContent).toBe('a');

			await act(async () => {
				eventEmitter.emit(eventName, eventContext, 'b');
			});

			expect(getByTestId('test-text').textContent).toBe('ab');
		});
	});
});
