import React from 'react';
import { eventEmitterFactory } from './event-emitter';
import { EventEmitter } from './events.types';

const getEventEmitterInstance = eventEmitterFactory();

export const EventsContext = React.createContext<EventEmitter>(
	getEventEmitterInstance()
);

type EventsContextProviderProps = {
	eventEmitter?: EventEmitter;
};

export const EventsContextProvider: React.FC<EventsContextProviderProps> = ({
	children,
	eventEmitter
}) => {
	return (
		<EventsContext.Provider value={eventEmitter || getEventEmitterInstance()}>
			{children}
		</EventsContext.Provider>
	);
};
