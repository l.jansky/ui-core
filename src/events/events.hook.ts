import React from 'react';
import { EventsContext } from './events.context';
import { EventContext, EventEmitter } from './events.types';
import { DataValue } from '../store';

const isInContext = (
	subscribedContext: EventContext,
	eventContext: EventContext
) => {
	if (subscribedContext.length > eventContext.length) {
		return false;
	}

	for (const i in subscribedContext) {
		if (subscribedContext[i] !== eventContext[i]) {
			return false;
		}
	}

	return true;
};

export const useEvents = (
	events: string[],
	context: EventContext,
	callback: (data: DataValue) => void
) => {
	const eventEmitter = React.useContext(EventsContext);
	const [fixedEvents, setFixedEvents] = React.useState(events);
	const callbackRef = React.useRef(callback);
	callbackRef.current = callback;

	React.useEffect(() => {
		if (events.join() !== fixedEvents.join()) {
			setFixedEvents(events);
		}
	}, [events]);

	React.useEffect(() => {
		const listener = (eventContext: EventContext, data: DataValue) => {
			if (isInContext(context, eventContext)) {
				callbackRef.current(data);
			}
		};

		fixedEvents.forEach(event => eventEmitter.on(event, listener));

		return () => {
			fixedEvents.forEach(event => {
				eventEmitter.removeListener(event, listener);
			});
		};
	}, [eventEmitter, fixedEvents]);
};

export const useEventEmitter = (): EventEmitter =>
	React.useContext(EventsContext);
