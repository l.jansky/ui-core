import EventEmitter3 from 'eventemitter3';
import { EventEmitter } from './events.types';

type GetEventEmitter = () => EventEmitter;

export const eventEmitterFactory = (): GetEventEmitter => {
	let eventEmitterInstance: EventEmitter3;
	return () => {
		if (!eventEmitterInstance) {
			eventEmitterInstance = new EventEmitter3();
		}

		return {
			emit: (event, context, data) => {
				eventEmitterInstance.emit(event, context, data);
			},
			on: (event, listener) => {
				eventEmitterInstance.on(event, listener);
			},
			removeListener: (event, listener) => {
				eventEmitterInstance.removeListener(event, listener);
			}
		};
	};
};
