import path from 'path';

export type PathSegment = string | number;

const stringNumbersToInt = (str: string): string | number => {
	const result = /^\+?([0-9]\d*)$/.test(str) ? parseInt(str) : str;
	return result;
};

export const unescapePath = (pathString: string) => {
	if (pathString.startsWith('//')) {
		return pathString.substring(1);
	}

	return pathString;
};

export const isPath = (pathString: string) => {
	return (
		(pathString.startsWith('/') && !pathString.startsWith('//')) ||
		pathString.startsWith('./') ||
		pathString.startsWith('../')
	);
};

export const getAbsolutePathArray = (
	pathString: string,
	parentPathArray: PathSegment[]
): PathSegment[] => {
	if (!pathString) {
		return parentPathArray;
	}

	const [pathStart, ...restOfPath] = pathString.split('/');
	if (pathStart === '' && !pathString.startsWith('//')) {
		return restOfPath.map(stringNumbersToInt);
	}

	if (pathStart === '.' || pathStart === '..') {
		const [, ...resolvedPath] = path
			.resolve('/', parentPathArray.join('/'), pathString)
			.split('/')
			.map(stringNumbersToInt);

		return resolvedPath;
	}

	throw 'Not path';
};
