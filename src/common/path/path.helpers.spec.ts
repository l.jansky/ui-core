import { getAbsolutePathArray, isPath, unescapePath } from './path.helpers';

describe('isPath', () => {
	it('should return true for /test/test', () => {
		expect(isPath('/test/test')).toBe(true);
	});

	it('should return true for ./test', () => {
		expect(isPath('./test')).toBe(true);
	});

	it('should return true for ../test', () => {
		expect(isPath('../test')).toBe(true);
	});

	it('should return false for test', () => {
		expect(isPath('test')).toBe(false);
	});

	it('should return false for empty string', () => {
		expect(isPath('')).toBe(false);
	});

	it('should return false for //test/test', () => {
		expect(isPath('//test/test')).toBe(false);
	});
});

describe('getAbsoluteDataPathArray', () => {
	it('should ignore parentPath if path starts with /', () => {
		const resultPath = getAbsolutePathArray('/absolute/path', [
			'this',
			'is',
			'ignored'
		]);
		expect(resultPath).toEqual(['absolute', 'path']);
	});

	it('should return path relative to parentPath if it starts with .', () => {
		const resultPath = getAbsolutePathArray('./relative/path', [
			'parent',
			'path'
		]);
		expect(resultPath).toEqual(['parent', 'path', 'relative', 'path']);
	});

	it('should return path relative to parent if it starts with ..', () => {
		const resultPath = getAbsolutePathArray('../relative/ignored/../path', [
			'parent',
			'path'
		]);
		expect(resultPath).toEqual(['parent', 'relative', 'path']);
	});

	it('should throw error if path does not start with /, ., ..', () => {
		expect(() => getAbsolutePathArray('not/path', [])).toThrow();
	});

	it('should throw error if path starts with //', () => {
		expect(() => getAbsolutePathArray('//not/path', [])).toThrow();
	});

	it('should return path with array index as number', () => {
		const resultPath = getAbsolutePathArray('./relative/2/path', [
			'parent',
			1,
			'path'
		]);
		expect(resultPath).toEqual(['parent', 1, 'path', 'relative', 2, 'path']);
	});
});

describe('unEscapePath', () => {
	it('should not remove first slash if path starts with only one slash', () => {
		const resultPath = unescapePath('/test/1');
		expect(resultPath).toEqual('/test/1');
	});

	it('should remove first slash if path starts with two slashes', () => {
		const resultPath = unescapePath('//test/1');
		expect(resultPath).toEqual('/test/1');
	});
});
