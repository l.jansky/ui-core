import React from 'react';

// TODO: probably can be deleted if not needed
export const useEffectWithoutMount = (
	effect: React.EffectCallback,
	deps: any[]
) => {
	const didMountRef = React.useRef(false);
	React.useEffect(() => {
		if (didMountRef.current) {
			return effect();
		} else {
			didMountRef.current = true;
		}
	}, [deps]);
};
