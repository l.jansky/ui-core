import React from 'react';
import { DEFAULT_PIPE_FUNCTIONS } from './functions';
import { GetPipeFunction, PipeContextProviderProps } from './pipe.types';
import { getPipeIO } from './pipe.helpers';

export const PipeContext = React.createContext<GetPipeFunction>(() => () =>
	Promise.resolve(getPipeIO())
);

export const PipeContextProvider: React.FunctionComponent<PipeContextProviderProps> = ({
	functions,
	children
}) => {
	const getPipeFunction: GetPipeFunction = name => {
		const allFunctions = {
			...DEFAULT_PIPE_FUNCTIONS,
			...functions
		};

		if (!allFunctions[name]) {
			throw `Pipe function ${name} not found`;
		}

		return allFunctions[name];
	};

	return (
		<PipeContext.Provider value={getPipeFunction}>
			{children}
		</PipeContext.Provider>
	);
};
