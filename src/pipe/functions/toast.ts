import { uuid } from 'uuidv4';
import { MODULE_OPENER_LIST_DATA_PATH } from '../../components/base-components/module-opener/module-opener';
import { ModuleOpenerItem } from '../../components/base-components/module-opener/module-opener.types';

import { PipeFunction } from '../pipe.types';

export const toast: PipeFunction = ({
	input,
	next,
	store,
	params: [variantParam]
}) => {
	const message = typeof input.value === 'string' ? input.value : '';
	const variant = variantParam && variantParam.value;
	const path = [MODULE_OPENER_LIST_DATA_PATH];
	const currentList = (store.getValue(path) as ModuleOpenerItem[]) || [];
	const currentListWithoutToasts = currentList.filter(
		item =>
			!item.childConfigs ||
			!item.childConfigs[0] ||
			item.childConfigs[0].name !== 'toast'
	);
	const contextUuid = uuid();
	const newItem: ModuleOpenerItem = {
		data: { message },
		context: [contextUuid],
		childConfigs: [
			{
				name: 'toast',
				attributes: {
					message,
					variant: variant as string
				}
			}
		]
	};
	store.setValue(path, [...currentListWithoutToasts, newItem as any]);
	setTimeout(() => {
		const currentList = (store.getValue(path) as ModuleOpenerItem[]) || [];
		const filteredList = currentList.filter(
			item => item.context[0] !== contextUuid
		);
		store.setValue(path, filteredList as any);
	}, 3000);

	return next(input.value);
};
