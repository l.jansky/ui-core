import { PipeFunction } from '../pipe.types';
import * as R from 'ramda';
import { MODULE_OPENER_LIST_DATA_PATH } from '../../components/base-components/module-opener/module-opener';
import { ModuleOpenerItem } from '../../components/base-components/module-opener/module-opener.types';
import { EventContext } from '../../events';

export const getOpenPipeFunction = (context: EventContext): PipeFunction => ({
	input,
	next,
	params: [module, contextPart],
	store
}) => {
	const path = [MODULE_OPENER_LIST_DATA_PATH];
	const contextPartValue = contextPart ? contextPart.value : '';
	const currentList = (store.getValue(path) as ModuleOpenerItem[]) || [];
	const newItem: ModuleOpenerItem = {
		module: module.value as string,
		data: R.clone(input.value),
		context: [...context, contextPartValue as string]
	};
	store.setValue(path, [...currentList, newItem as any]);
	// TODO: probably could return promise which will call next on resolve and will be listening for event 'continue' from opened module
	return next(input.value);
};
