import { PipeFunction } from '../pipe.types';

export const objectSet: PipeFunction = ({
	input: { value },
	params: [nameParam, valueParam],
	next
}) => {
	const res = {
		...(typeof value === 'object' ? value : {}),
		[nameParam.value as string]: valueParam.value
	};
	return next(res);
};
