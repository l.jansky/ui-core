import { PipeFunction } from '../pipe.types';

export const append: PipeFunction = ({ input, next, params: [param] }) => {
	const list = input.value;

	let paramValue = param && param.value;
	if (paramValue === '[]') {
		paramValue = [];
	}

	if (paramValue === '{}') {
		paramValue = {};
	}

	if (!Array.isArray(list)) {
		throw new Error('Invalid list value');
	}

	const result = [...list, paramValue as any];
	return next(result);
};
