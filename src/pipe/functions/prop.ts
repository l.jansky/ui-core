import { PipeFunction } from '../pipe.types';

export const prop: PipeFunction = ({
	input: { value },
	next,
	params: [prop]
}) => {
	const propValue = value && (value as any)[prop.value as string];
	return next(propValue);
};
