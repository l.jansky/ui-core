import { PipeFunction } from '../pipe.types';

export const pick: PipeFunction = ({ input, next, params }) => {
	const inputValue = input.value;
	if (typeof inputValue !== 'object' || Array.isArray(inputValue)) {
		throw new Error('Input value for pick is not object');
	}

	const result = params.reduce((acc, curr) => {
		if (typeof curr.value === 'string') {
			return {
				...acc,
				[curr.value]: inputValue[curr.value]
			};
		}
		return acc;
	}, {});

	return next(result);
};
