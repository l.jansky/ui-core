import { PipeFunction } from '../pipe.types';

export const operator: PipeFunction = ({
	input: { value },
	next,
	params: [operatorParam]
}) => {
	const operatorValue = operatorParam.value;

	return next(`${operatorValue}(${value})`);
};
