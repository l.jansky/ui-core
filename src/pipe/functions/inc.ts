import { PipeFunction } from '../pipe.types';

export const inc: PipeFunction = ({ input, next, params: [incParam] }) => {
	let value = input.value;
	let incValue = (incParam && incParam.value) || 1;
	if (typeof incValue === 'string') {
		incValue = parseInt(incValue);
	}

	if (typeof value === 'string') {
		value = parseInt(value);
	}

	if (typeof value !== 'number') {
		throw 'Invalid input value for inc pipe function';
	}

	if (typeof incValue !== 'number') {
		throw 'Invalid param value for inc pipe function';
	}

	return next(value + incValue);
};
