import { PipeFunctions } from '../pipe.types';
import { log } from './log';
import { inc } from './inc';
import { mult } from './mult';
import { delay } from './delay';
import { store } from './store';
import { storeToggle } from './store-toggle';
import { not } from './not';
import { uppercase } from './uppercase';
import { put } from './put';
import { deleteFunction } from './delete';
import { post } from './post';
import { get } from './get';
import { value } from './value';
import { find } from './find';
import { prop } from './prop';
import { eq } from './eq';
import { appendTo } from './append-to';
import { objectSet } from './objectSet';
import { operator } from './operator';
import { close } from './close';
import { path } from './path';
import { wrap } from './wrap';
import { append } from './append';
import { and } from './and';
import { pick } from './pick';
import { allSet } from './allSet';
import { assign } from './assign';
import { toBoolean } from './to-boolean';
import { omit } from './omit';
import { omitObjects } from './omitObjects';
import { defaultFunction } from './default';
import { last } from './last';
import { unsetPath } from './unset-path';
import { concat } from './concat';
import { toast } from './toast';
import { concatTo } from './concat-to';
export { getOpenPipeFunction } from './open.factory';
export { getEmitPipeFunction } from './emit.factory';

export const DEFAULT_PIPE_FUNCTIONS: PipeFunctions = {
	log,
	inc,
	mult,
	delay,
	store,
	not,
	uppercase,
	put,
	delete: deleteFunction,
	post,
	get,
	value,
	find,
	prop,
	eq,
	appendTo,
	objectSet,
	operator,
	storeToggle,
	close,
	path,
	wrap,
	append,
	and,
	pick,
	allSet,
	assign,
	toBoolean,
	omit,
	omitObjects,
	default: defaultFunction,
	last,
	unsetPath,
	concat,
	toast,
	concatTo
};
