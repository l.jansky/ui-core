import { PipeFunction } from '../pipe.types';

export const mult: PipeFunction = ({ input: { value }, next }) => {
	if (typeof value === 'number') {
		return next(value * 2);
	}

	if (typeof value === 'string') {
		return next(parseInt(value) * 2);
	}

	throw 'Not number ' + value;
};
