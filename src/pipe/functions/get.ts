import { PipeFunction } from '../pipe.types';
import { replaceParams } from '../pipe.helpers';

export const get: PipeFunction = async ({
	input: { value },
	next,
	params: [endpoint]
}) => {
	const url = replaceParams(endpoint.value as string)(value);
	const response = await fetch(url);
	const responseData = await response.json();
	return next(responseData);
};
