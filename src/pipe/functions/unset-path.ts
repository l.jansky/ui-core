import { PipeFunction } from '../pipe.types';
import { PathSegment } from '../../common';

export const unsetPath: PipeFunction = ({ input, next, store }) => {
	const path = input.value;
	if (!Array.isArray(path)) {
		throw new Error('Invalid path for unset');
	}

	store.setValue(path as PathSegment[], undefined);
	return next(path);
};
