import { PipeFunction } from '../pipe.types';

export const concatTo: PipeFunction = ({
	input: { value },
	next,
	params: [{ value: paramList }]
}) => {
	if (!Array.isArray(value)) {
		throw new Error('Invalid input value');
	}

	if (!Array.isArray(paramList)) {
		throw new Error('Invalid param value');
	}

	return next([...paramList, ...value]);
};
