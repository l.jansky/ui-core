import { PipeFunction } from '../pipe.types';
import { EventEmitter, EventContext } from '../../events';

export const getEmitPipeFunction = (
	eventEmitter: EventEmitter,
	context: EventContext
): PipeFunction => ({ input: { value }, next, params: [eventName] }) => {
	const eventNameValue = eventName && eventName.value;
	if (!eventNameValue) {
		throw 'Missing event name';
	}

	if (typeof eventNameValue !== 'string') {
		throw new Error('Invalid event name');
	}

	eventEmitter.emit(eventNameValue, context, value);
	return next(value);
};
