import { PipeFunction } from '../pipe.types';

export const omit: PipeFunction = ({ input, next, params }) => {
	const inputValue = input.value;
	if (typeof inputValue !== 'object' || Array.isArray(inputValue)) {
		throw new Error('Input value for pick is not object');
	}

	const result = params.reduce((acc, curr) => {
		if (typeof curr.value === 'string') {
			const result = { ...acc };
			delete result[curr.value];
			return result;
		}
		return acc;
	}, inputValue);

	return next(result);
};
