import { PipeFunction } from '../pipe.types';
import { PathSegment } from '../../common';
// TODO: resolve this circular dependency
// import { MODULE_OPENER_LIST_DATA_PATH } from '../../components';

export const close: PipeFunction = ({ input: { value }, next, store }) => {
	if (!value || !Array.isArray(value)) {
		throw 'Invalid path';
	}

	const [MODULE_OPENER_LIST_DATA_PATH, openedId] = value as PathSegment[];

	store.setValue([MODULE_OPENER_LIST_DATA_PATH, openedId], undefined);
	return next(value);
};
