import { PipeFunction } from '../pipe.types';
import { PathSegment } from '../../common';

export const store: PipeFunction = ({
	input: { value, path },
	next,
	params: [{ path: paramPath } = { path: undefined }],
	store
}) => {
	if (!path && !paramPath) {
		throw 'Invalid path';
	}
	store.setValue(paramPath || (path as PathSegment[]), value);
	return next(value);
};
