import { PipeFunction } from '../pipe.types';

export const last: PipeFunction = ({ input, next }) => {
	const list = input.value;

	if (!Array.isArray(list)) {
		throw new Error('Invalid list value');
	}

	const lastValue = list[list.length - 1];
	return next(lastValue);
};
