import { PipeFunction } from '../pipe.types';

export const omitObjects: PipeFunction = ({ input, next }) => {
	const inputValue = input.value;
	if (typeof inputValue !== 'object' || Array.isArray(inputValue)) {
		throw new Error('Input value for pick is not object');
	}

	const result: any = {};
	for (const i in inputValue) {
		if (typeof inputValue[i] !== 'object') {
			result[i] = inputValue[i];
		}
	}

	return next(result);
};
