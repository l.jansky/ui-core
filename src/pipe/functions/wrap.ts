import { PipeFunction } from '../pipe.types';

export const wrap: PipeFunction = ({
	input: { value },
	params: [{ value: key }],
	next
}) => {
	const result = {
		[key as string]: value
	};
	return next(result);
};
