import { PipeFunction } from '../pipe.types';

export const assign: PipeFunction = ({
	input,
	next,
	params: [nameParam, valueParam]
}) => {
	const inputObject = input.value;
	const attributeName = nameParam.value;
	const attributeValue = valueParam.value;
	if (typeof inputObject !== 'object' || Array.isArray(inputObject)) {
		return next({ [attributeName as string]: attributeValue });
	}

	return next({
		...inputObject,
		[attributeName as string]: attributeValue
	});
};
