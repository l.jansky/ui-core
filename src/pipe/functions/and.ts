import { PipeFunction } from '../pipe.types';

export const and: PipeFunction = ({ input, next, params: [param] }) => {
	const inputValue = input.value;
	const paramValue = param && param.value;

	return next(!!(inputValue && paramValue));
};
