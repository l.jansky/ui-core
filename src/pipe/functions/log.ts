import { PipeFunction } from '../pipe.types';

export const log: PipeFunction = ({ input: { value }, next }) => {
	console.log(value);
	return next(value);
};
