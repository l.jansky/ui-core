import { PipeFunction } from '../pipe.types';

export const find: PipeFunction = ({
	input: { value },
	next,
	params: [valueProp, list]
}) => {
	const found = (list.value as any[]).find(
		item => item[valueProp.value as string] === value
	);

	return next(found);
};
