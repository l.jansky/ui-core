import { PipeFunction } from '../pipe.types';
import { replaceParams } from '../pipe.helpers';

export const post: PipeFunction = async ({
	input: { value },
	next,
	params: [endpoint]
}) => {
	const url = replaceParams(endpoint.value as string)(value);
	const response = await fetch(url, {
		method: 'POST',
		headers: {
			Accept: 'application/json',
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(value)
	});
	const responseData = await response.json();
	return next(responseData);
};
