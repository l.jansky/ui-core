import { PipeFunction } from '../pipe.types';

export const allSet: PipeFunction = ({ input, next, params }) => {
	const inputObject = input.value;
	if (typeof inputObject !== 'object' || Array.isArray(inputObject)) {
		return next(undefined);
	}

	const allAttributesSet = params.reduce((acc, curr) => {
		const isAttributeDefined =
			typeof inputObject[curr.value as string] !== 'undefined';
		return acc && isAttributeDefined;
	}, true);

	return next(allAttributesSet ? inputObject : undefined);
};
