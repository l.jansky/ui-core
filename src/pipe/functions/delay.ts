import { PipeFunction } from '../pipe.types';

export const delay: PipeFunction = ({
	input: { value },
	next,
	params: [delayParam]
}) => {
	let delayValue = (delayParam && delayParam.value) || 1000;
	if (typeof delayValue === 'string') {
		delayValue = parseInt(delayValue);
	}

	if (typeof delayValue !== 'number') {
		throw 'Invalid param value for inc pipe function';
	}

	return new Promise(resolve => {
		setTimeout(() => {
			resolve(next(value));
		}, delayValue as number);
	});
};
