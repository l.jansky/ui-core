import { PipeFunction } from '../pipe.types';

export const defaultFunction: PipeFunction = ({
	input,
	next,
	params: [param]
}) => {
	let paramValue = param && param.value;
	if (paramValue === '[]') {
		paramValue = [];
	}

	if (paramValue === '{}') {
		paramValue = {};
	}

	return next(typeof input.value !== 'undefined' ? input.value : paramValue);
};
