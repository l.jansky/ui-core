import { PipeFunction } from '../pipe.types';

export const uppercase: PipeFunction = ({ input: { value }, next }) => {
	if (typeof value === 'string') {
		return next(value.toUpperCase());
	}

	return next(value);
};
