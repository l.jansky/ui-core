import { PipeFunction } from '../pipe.types';

export const appendTo: PipeFunction = ({
	input: { value },
	next,
	params: [{ value: paramList }]
}) => {
	const list: any[] = [...(paramList as any[]), value];
	return next(list);
};
