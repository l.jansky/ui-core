import { PipeFunction } from '../pipe.types';

export const not: PipeFunction = ({ input: { value }, next }) => {
	return next(!value);
};
