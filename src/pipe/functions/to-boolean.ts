import { PipeFunction } from '../pipe.types';

export const toBoolean: PipeFunction = ({ input, next, params: [param] }) => {
	const inputValue = param ? param.value : input.value;
	return next(!!inputValue);
};
