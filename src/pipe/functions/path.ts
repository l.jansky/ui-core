import { PipeFunction } from '../pipe.types';

export const path: PipeFunction = ({ next, params: [param] }) => {
	const paramPath = param && param.path;
	if (!paramPath) {
		throw new Error('Invalid path');
	}

	return next(paramPath);
};
