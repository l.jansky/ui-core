import { PipeFunction } from '../pipe.types';
import { replaceParams } from '../pipe.helpers';

export const deleteFunction: PipeFunction = async ({
	input: { value },
	next,
	params: [{ value: endpoint }]
}) => {
	const url = replaceParams(endpoint as string)(value);
	const response = await fetch(url, {
		method: 'DELETE'
	});
	const responseData = await response.json();
	return next(responseData);
};
