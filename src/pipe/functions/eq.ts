import { PipeFunction } from '../pipe.types';

export const eq: PipeFunction = ({
	input: { value },
	params: [{ value: paramValue }],
	next
}) => {
	// TODO: not listening to param change (maybe not problem here)
	return next(value === paramValue);
};
