import { PipeFunction } from '../pipe.types';

const separators: { [key: string]: string } = {
	'@space': ' ',
	'@comma': ', '
};

export const concat: PipeFunction = ({
	input,
	next,
	params: [param, separator]
}) => {
	console.log('BLA');
	const leftString = typeof input.value === 'string' ? input.value : '';
	const rightString =
		param && typeof param.value === 'string' ? param.value : '';

	const separatorValue =
		typeof separator.value === 'string' ? separator.value : '';
	const separatorString = separators[separatorValue] || separatorValue;

	return next(leftString + rightString ? separatorString + rightString : '');
};
