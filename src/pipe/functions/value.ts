import { PipeFunction } from '../pipe.types';

export const value: PipeFunction = ({ next, params: [param] }) => {
	const paramValue = param && param.value;
	if (paramValue === '[]') {
		return next([]);
	}

	if (paramValue === '{}') {
		return next({});
	}

	return next(paramValue);
};
