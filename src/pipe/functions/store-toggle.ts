import { PipeFunction } from '../pipe.types';

export const storeToggle: PipeFunction = ({ next, params: [param], store }) => {
	if (!param || !param.path) {
		throw 'Invalid path';
	}

	store.setValue(param.path, !param.value);
	return next(!param.value);
};
