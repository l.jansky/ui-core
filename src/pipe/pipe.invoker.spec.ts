import { Pipe, GetPipeFunction } from './pipe.types';
import { getInvokePipe } from './pipe.invoker';
import { getPipeIO } from './pipe.helpers';
import { DEFAULT_PIPE_FUNCTIONS } from './functions';
import { getStore } from '../store/store';

describe('PipeInvoker', () => {
	const getPipeFunction: GetPipeFunction = name => DEFAULT_PIPE_FUNCTIONS[name];

	const store = getStore({
		test: {
			value: 3
		}
	});

	const invokePipe = getInvokePipe(getPipeFunction, store);
	it('should invoke pipe on input value', async () => {
		const pipe: Pipe = [
			{
				name: 'inc',
				params: []
			},
			{
				name: 'mult',
				params: []
			}
		];

		const { value } = await invokePipe(pipe, getPipeIO('1'));
		expect(value).toEqual(4);
	});

	it('should invoke pipe with parameter', async () => {
		const pipe: Pipe = [
			{
				name: 'inc',
				params: [getPipeIO('3')]
			}
		];

		const { value } = await invokePipe(pipe, getPipeIO('1'));
		expect(value).toEqual(4);
	});

	it('should invoke async pipe function', async () => {
		const pipe: Pipe = [
			{
				name: 'delay',
				params: [getPipeIO('4000')]
			},
			{
				name: 'inc',
				params: [getPipeIO('5')]
			}
		];

		const { value } = await invokePipe(pipe, getPipeIO('1'));
		expect(value).toEqual(6);
	});
});
