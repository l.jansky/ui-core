import UrlPattern from 'url-pattern';
import { RawPipe, Pipe, PipeIO } from './pipe.types';
import { PathSegment, isPath, getAbsolutePathArray } from '../common';
import { getDataValue, Store, DataValue } from '../store';

export const getMapPipeInputsToData = (
	parentDataPath: PathSegment[],
	store: Store
) => (pipe: RawPipe): Pipe => {
	return pipe.map(pipeFunction => ({
		name: pipeFunction.name,
		params: pipeFunction.params.map(param => ({
			get value() {
				return getDataValue(param, parentDataPath, store.getData());
			},
			path: isPath(param) ? getAbsolutePathArray(param, parentDataPath) : []
		}))
	}));
};

export const replaceParams = (url: string) => (params: any) => {
	const pattern = new UrlPattern(url, {
		segmentNameCharset: 'a-zA-Z0-9_-'
	});
	return pattern.stringify(params);
};

export const getPipeIO: (value?: DataValue, path?: PathSegment[]) => PipeIO = (
	value,
	path
) => ({
	value,
	path
});
