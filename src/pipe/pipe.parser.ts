import { RawPipeFunction, RawPipe } from './pipe.types';

export const parseParams = (paramsString: string): string[] => {
	const params: string[] = [];
	let param = '';
	let opened = 0;
	let closed = 0;

	for (let i = 0; i < paramsString.length; i++) {
		const c = paramsString[i];

		if (c === '(') {
			opened++;
		}

		if (c === ')') {
			closed++;
		}

		if (c === ',' && opened === closed) {
			params.push(param.trim());
			param = '';
			continue;
		}
		param += c;
		if (i === paramsString.length - 1) {
			params.push(param.trim());
		}
	}
	return params;
};

export const parsePipe = (
	functionsString: string,
	pipeDelimiter: string
): RawPipe => {
	const functions: RawPipeFunction[] = [];
	let name = '';
	let paramsString = '';

	let opened = 0;
	let closed = 0;
	const [...functionsChars] = functionsString;

	for (let i = 0; i < functionsChars.length; i++) {
		const c = functionsChars[i];

		if (c === '(') {
			opened++;
			if (opened === closed + 1) {
				continue;
			}
		}

		if (c === ')') {
			closed++;
		}

		if (
			(c === pipeDelimiter || i === functionsChars.length - 1) &&
			opened === closed
		) {
			if (i === functionsChars.length - 1 && c !== ')') {
				name += c;
			}

			functions.push({
				name,
				params: parseParams(paramsString)
			});
			name = '';
			paramsString = '';
			opened = 0;
			closed = 0;
			continue;
		}

		if (opened === closed) {
			if (c !== ')') {
				name += c;
			}
		} else {
			paramsString += c;
		}
	}

	return functions;
};
