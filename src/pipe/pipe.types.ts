import { DataValue, Store } from '../store';
import { PathSegment } from '../common';

export type InvokePipe = (pipe: Pipe, input: PipeIO) => Promise<PipeIO>;

export type PipeFunctionParams = {
	input: PipeIO;
	next: PipeNext;
	params: PipeIO[];
	store: Store;
};

export type PipeFunction = (params: PipeFunctionParams) => Promise<PipeIO>;

export type PipeNext = (value: DataValue) => Promise<PipeIO>;

export type GetPipeFunction = (name: string) => PipeFunction;

export type PipeFunctionDefinition = {
	name: string;
	params: PipeIO[];
};

export type PipeIO = {
	value: DataValue;
	path?: PathSegment[];
};

export type RawPipeFunction = {
	name: string;
	params: string[];
};

export type RawPipe = RawPipeFunction[];

export type Pipe = PipeFunctionDefinition[];

export type PipeFunctions = {
	[name: string]: PipeFunction;
};

export type PipeContextProviderProps = {
	functions: PipeFunctions;
};
