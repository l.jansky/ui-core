import { parsePipe } from './pipe.parser';

describe('PipeParser', () => {
	it('should parse pipe without parameters', () => {
		const pipeString = 'function1|function2';
		const result = parsePipe(pipeString, '|');
		expect(result).toEqual([
			{
				name: 'function1',
				params: []
			},
			{
				name: 'function2',
				params: []
			}
		]);
	});

	it('should parse pipe with parameters', () => {
		const pipeString =
			'function1(parameter1, parameter2)|function2(parameter3)';
		const result = parsePipe(pipeString, '|');
		expect(result).toEqual([
			{
				name: 'function1',
				params: ['parameter1', 'parameter2']
			},
			{
				name: 'function2',
				params: ['parameter3']
			}
		]);
	});
});
