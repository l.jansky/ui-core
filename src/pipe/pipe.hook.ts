import { useContext, useState, useEffect } from 'react';
import { StoreContext } from '../store/store.context';
import { PipeContext } from './pipe.context';
import { getInvokePipe } from './pipe.invoker';
import { parsePipe } from './pipe.parser';
import { PathSegment } from '../common';
import { getMapPipeInputsToData, getPipeIO } from './pipe.helpers';
import { PipeIO, GetPipeFunction } from './pipe.types';
import { DataValue } from '../store';
import { useEventEmitter, EventContext } from '../events';
import { getEmitPipeFunction, getOpenPipeFunction } from './functions';

export const usePipeFunctionFactory = (
	context: EventContext
): GetPipeFunction => {
	const getPipeFunction = useContext(PipeContext);
	const eventEmitter = useEventEmitter();

	return name => {
		if (name === 'emit') {
			return getEmitPipeFunction(eventEmitter, context);
		}

		if (name === 'open') {
			return getOpenPipeFunction(context);
		}

		return getPipeFunction(name);
	};
};

export const useInvokePipe = (
	pipeString: string,
	parentDataPath: PathSegment[],
	context: EventContext
) => {
	const getPipeFunction = usePipeFunctionFactory(context);
	const store = useContext(StoreContext);
	const mapInputsToData = getMapPipeInputsToData(parentDataPath, store);
	const pipe = mapInputsToData(parsePipe(pipeString, '|'));

	const invokePipe = (input: PipeIO) =>
		getInvokePipe(getPipeFunction, store)(pipe, input);
	return invokePipe;
};

export const usePipeTransformation = (
	pipeString: string,
	value: DataValue,
	parentDataPath: PathSegment[],
	context: EventContext
) => {
	const invokePipe = useInvokePipe(pipeString, parentDataPath, context);
	const [transformedValue, setTransformedValue] = useState<DataValue>();

	useEffect(() => {
		invokePipe(getPipeIO(value)).then(result => {
			setTransformedValue(result.value);
		});
	}, [value]);
	return transformedValue;
};
