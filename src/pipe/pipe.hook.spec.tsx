import React from 'react';
import { renderHook, act } from '@testing-library/react-hooks';
import { StoreContextProvider, getStore, Store } from '../store';
import { PipeContextProvider } from './pipe.context';
import { useInvokePipe } from './pipe.hook';
import { getPipeIO } from './pipe.helpers';
import {
	EventsContextProvider,
	EventEmitter,
	eventEmitterFactory
} from '../events';
import { MODULE_OPENER_LIST_DATA_PATH } from '../components/base-components/module-opener/module-opener';

const mockPipeFunctions = {
	test: jest.fn()
};

describe('usePipe hook', () => {
	const initialData = {
		rootParamValue: 'root-param-value',
		test: {
			subValue: 'test-sub-value',
			paramValue: 'param-value'
		}
	};

	const getWrapper = (eventEmitter?: EventEmitter): [React.FC, Store] => {
		const store = getStore(initialData);
		return [
			({ children }) => (
				<EventsContextProvider eventEmitter={eventEmitter}>
					<PipeContextProvider functions={mockPipeFunctions}>
						<StoreContextProvider store={store}>
							{children}
						</StoreContextProvider>
					</PipeContextProvider>
				</EventsContextProvider>
			),
			store
		];
	};

	beforeEach(() => {
		jest.clearAllMocks();
	});

	it('should call pipe with input data', () => {
		const [wrapper] = getWrapper();
		const { result } = renderHook(() => useInvokePipe('test', ['test'], []), {
			wrapper
		});

		const input = getPipeIO(initialData.test.subValue, ['test', 'subValue']);

		act(() => {
			result.current(input);
		});

		expect(mockPipeFunctions.test).toHaveBeenCalledWith(
			expect.objectContaining({
				input
			})
		);
	});

	it('should call pipe with params from data', () => {
		const [wrapper] = getWrapper();
		const { result } = renderHook(
			() => useInvokePipe('test(./paramValue,/rootParamValue)', ['test'], []),
			{
				wrapper
			}
		);

		act(() => {
			result.current(
				getPipeIO(initialData.test.subValue, ['test', 'subValue'])
			);
		});

		expect(mockPipeFunctions.test).toHaveBeenCalledWith(
			expect.objectContaining({
				input: {
					value: initialData.test.subValue,
					path: ['test', 'subValue']
				},
				params: [
					{
						value: initialData.test.paramValue,
						path: ['test', 'paramValue']
					},
					{
						value: initialData.rootParamValue,
						path: ['rootParamValue']
					}
				]
			})
		);
	});

	it('should emit event with input data and context', () => {
		const eventEmitter = eventEmitterFactory()();
		const [wrapper] = getWrapper(eventEmitter);
		const emitSpy = jest.spyOn(eventEmitter, 'emit');
		const eventName = 'testEventName';
		const context = ['context'];
		const { result } = renderHook(
			() => useInvokePipe(`emit(${eventName})`, ['test'], context),
			{
				wrapper
			}
		);

		const input = getPipeIO(initialData.test.subValue);

		act(() => {
			result.current(input);
		});

		expect(emitSpy).toHaveBeenCalledWith(
			eventName,
			context,
			initialData.test.subValue
		);
	});

	it('should add (open function) item to opener list', async () => {
		const [wrapper, store] = getWrapper();
		const modulePath = 'http://localhost/module';
		const contextPart = 'opened-context';
		const context = ['context'];
		const data = { test: 1 };
		const { result } = renderHook(
			() =>
				useInvokePipe(`open(${modulePath},${contextPart})`, ['test'], context),
			{ wrapper }
		);

		const input = getPipeIO(data);

		await act(async () => {
			await result.current(input);
		});

		const openerStoreData = store.getValue([MODULE_OPENER_LIST_DATA_PATH]);
		expect(openerStoreData).toEqual([
			{
				module: modulePath,
				context: [...context, contextPart],
				data: data
			}
		]);
	});
});
