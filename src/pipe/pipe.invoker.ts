import { Store } from '../store';
import { GetPipeFunction, InvokePipe, PipeNext } from './pipe.types';
import { getPipeIO } from './pipe.helpers';

export const getInvokePipe = (
	getPipeFunction: GetPipeFunction,
	store: Store
): InvokePipe => (functions, input) => {
	const [currentFunctionDefinition, ...restOfFunctionDefinitions] = functions;
	if (!currentFunctionDefinition) {
		return Promise.resolve(input);
	}

	const pipeFunction = getPipeFunction(currentFunctionDefinition.name);

	const invokePipe = getInvokePipe(getPipeFunction, store);
	const next: PipeNext = value =>
		invokePipe(restOfFunctionDefinitions, getPipeIO(value, input.path));

	const params = currentFunctionDefinition.params;
	return pipeFunction({ input, next, params, store });
};
