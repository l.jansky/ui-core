type AnyFunction = (...args: any[]) => any;

export const throttle = (
	handler: AnyFunction,
	throttleTime: number
): AnyFunction => {
	let lastCall: number | null = null;
	return (...args) => {
		const actualTime = Date.now();

		if (!lastCall || actualTime - lastCall > throttleTime) {
			console.log(
				'CALL THROTTLED FUNCTION',
				lastCall,
				actualTime,
				lastCall && actualTime - lastCall
			);
			lastCall = actualTime;
			return handler(...args);
		}
	};
};
