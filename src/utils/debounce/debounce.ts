type AnyFunction = (...args: any[]) => any;

export const debounce = (
	handler: AnyFunction,
	debounceTime: number
): AnyFunction => {
	if (!debounceTime) {
		return handler;
	}

	let lastCallTimer: any = null;
	let lastCallTime: number = Date.now();
	return (...args) => {
		const actualCallTime = Date.now();
		if (lastCallTimer && actualCallTime - lastCallTime < debounceTime) {
			clearTimeout(lastCallTimer);
		}

		lastCallTime = actualCallTime;

		lastCallTimer = setTimeout(() => {
			handler(...args);
		}, debounceTime);
	};
};
