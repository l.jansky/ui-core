import React from 'react';
import { StoreContextProvider, getStore } from '../../store';
import { Store } from '../../store/store';
import { Data } from '../../store';
import { PipeContextProvider, PipeFunctions } from '../../pipe';
import { DEFAULT_PIPE_FUNCTIONS } from '../../pipe/functions';
import { EventsContextProvider, EventEmitter } from '../../events';
import { ComponentsContextProvider } from '../../components/components.context';

type AppTreeOptions = {
	initialStoreData?: Data;
	pipeFunctions?: PipeFunctions;
	eventEmitter?: EventEmitter;
};

export const getAppTree = (
	subTree: JSX.Element,
	{
		initialStoreData = {},
		pipeFunctions = DEFAULT_PIPE_FUNCTIONS,
		eventEmitter
	}: AppTreeOptions = {}
): [JSX.Element, () => Store] => {
	const store = getStore(initialStoreData);
	const tree = (
		<ComponentsContextProvider componentsDefinitions={{}}>
			<EventsContextProvider eventEmitter={eventEmitter}>
				<PipeContextProvider functions={pipeFunctions}>
					<StoreContextProvider store={store}>{subTree}</StoreContextProvider>
				</PipeContextProvider>
			</EventsContextProvider>
		</ComponentsContextProvider>
	);

	const getCurrentStore = (): Store => {
		return store;
	};

	return [tree, getCurrentStore];
};
