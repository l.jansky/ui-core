import { useState } from 'react';
import UrlPattern from 'url-pattern';
import { DataValuesObject } from '.';
import { DataValue } from './store.types';

const replaceParams = (params: DataValuesObject) => (url: string) => {
	const pattern = new UrlPattern(url.replace('://', '\\://'));
	try {
		return pattern.stringify(params);
	} catch (err) {
		return;
	}
};

export type FetchMethod = 'GET' | 'POST' | 'PUT' | 'DELETE';

type FetchOptions = {
	method?: FetchMethod;
	query?: DataValuesObject;
	params?: DataValuesObject;
	body?: DataValue;
};

export const useFetch = <T>(
	callback: (data: T) => void,
	errorCallback?: (error: any) => void
) => {
	const [isLoading, setLoading] = useState(false);

	const doFetch = (
		pattern: string,
		{ query, params, body, method = 'GET' }: FetchOptions = {}
	) => {
		const fetchCall = async () => {
			setLoading(true);
			const paramsObject = typeof params === 'object' ? params : undefined;
			const bodyObject = typeof body === 'object' ? body : undefined;

			const urlString = replaceParams(paramsObject || bodyObject || {})(
				pattern
			);

			if (!urlString) {
				setLoading(false);
				return;
			}

			const url = new URL(urlString, window.location.origin);
			url.search = new URLSearchParams(query).toString();

			const requestBody =
				(['POST', 'PUT'].includes(method) &&
					bodyObject &&
					JSON.stringify(bodyObject)) ||
				undefined;

			const response = await fetch(url.toString(), {
				method,
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json'
				},
				body: requestBody
			});
			const responseData = await response.json();
			if (response.ok) {
				callback(responseData);
			} else if (errorCallback) {
				errorCallback(responseData);
			}

			setLoading(false);
		};

		fetchCall();
	};

	return {
		doFetch,
		isLoading
	};
};
