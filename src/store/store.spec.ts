import { getStore } from './store';

describe('Data store', () => {
	it('should get value from store', () => {
		const store = getStore({ a: { b: 1 } });
		const value = store.getValue(['a', 'b']);
		expect(value).toBe(1);
	});

	it('should call callback on value change', () => {
		const store = getStore({});
		const callback = jest.fn();
		const dataPath = ['test', 'a'];

		store.subscribe(dataPath, callback);
		store.setValue(dataPath, { test: 1 });

		expect(callback).toHaveBeenCalledWith(
			{ test: 1 },
			{ test: { a: { test: 1 } } },
			dataPath
		);
	});

	it('should not call callback if different path is set', () => {
		const store = getStore({});
		const callback = jest.fn();

		store.subscribe(['a'], callback);
		store.setValue(['b'], 1);

		expect(callback).not.toHaveBeenCalled();
	});

	it('should call all parent callbacks on child value change', () => {
		const store = getStore({});
		const rootCallback = jest.fn();
		const level1Callback = jest.fn();
		const valueCallback = jest.fn();

		store.subscribe([], rootCallback);
		store.subscribe(['level1'], level1Callback);
		store.subscribe(['level1', 'value'], valueCallback);
		store.setValue(['level1', 'value'], 1);

		const expectedResultData = {
			level1: {
				value: 1
			}
		};

		expect(valueCallback).toHaveBeenCalledWith(1, expectedResultData, [
			'level1',
			'value'
		]);
		expect(level1Callback).toHaveBeenCalledWith(
			{ value: 1 },
			expectedResultData,
			['level1', 'value']
		);
		expect(rootCallback).toHaveBeenCalledWith(
			expectedResultData,
			expectedResultData,
			['level1', 'value']
		);
	});

	it('should call all child callbacks when parent change their value', () => {
		const store = getStore({});
		const rootCallback = jest.fn();
		const level1Callback = jest.fn();
		const valueCallback = jest.fn();

		store.subscribe([], rootCallback);
		store.subscribe(['level1'], level1Callback);
		store.subscribe(['level1', 'value'], valueCallback);
		store.setValue([], { level1: { value: 1 } });

		const expectedResultData = {
			level1: {
				value: 1
			}
		};

		expect(valueCallback).toHaveBeenCalledWith(1, expectedResultData, []);
		expect(level1Callback).toHaveBeenCalledWith(
			{ value: 1 },
			expectedResultData,
			[]
		);
		expect(rootCallback).toHaveBeenCalledWith(
			expectedResultData,
			expectedResultData,
			[]
		);
	});

	it('should unsubscribe from store path to stop calling callback', () => {
		const store = getStore({});
		const callback = jest.fn();

		const unsubscribe = store.subscribe(['a', 'b'], callback);
		store.setValue(['a', 'b'], { c: 1 });
		unsubscribe();
		store.setValue(['a', 'b'], { c: 2 });
		store.setValue(['a'], { b: { c: 3 } });
		store.setValue(['a', 'b', 'c'], 4);
		expect(callback).toHaveBeenCalledTimes(1);
	});
});
