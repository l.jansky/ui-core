import React from 'react';
import { Store, getStore } from './store';

export const StoreContext = React.createContext<Store>(getStore());

type StoreContextProviderProps = {
	store: Store;
};

export const StoreContextProvider: React.FC<StoreContextProviderProps> = ({
	children,
	store
}) => {
	return (
		<StoreContext.Provider value={store}>{children}</StoreContext.Provider>
	);
};
