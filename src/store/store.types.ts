import { PathSegment } from '../common/path/path.helpers';

export type DataValueType = Data | string | number | boolean | undefined;

export type DataValue = DataValueType | DataValueType[];

export type Data = {
	[key: string]: DataValue;
};

export type SetData = (data: Data) => void;
export type SetDataPath = (dataPath: PathSegment[], value: DataValue) => void;

export type DataHookValue = {
	data: Data;
	setData: SetData;
	value: DataValue;
	setValue: (value: DataValue) => void;
	dataPath: PathSegment[];
};

export type SetStorePathValue = (
	value: DataValue,
	overrideValue?: PathSegment[]
) => void;

export type StoreHookValue = [DataValue, SetStorePathValue, PathSegment[]];

export type PathAttributes = {
	[name: string]: string;
};

export type DataValuesObject = {
	[name: string]: any;
};
