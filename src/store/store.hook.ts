import { useContext, useState, useEffect, useCallback } from 'react';
import { StoreContext } from './store.context';
import { Store } from './store';
import {
	PathSegment,
	isPath,
	getAbsolutePathArray,
	unescapePath
} from '../common/path/path.helpers';
import { DataValue, StoreHookValue } from './store.types';
import { noop } from '../common';
import { usePipeTransformation } from '../pipe';
import { EventContext } from '../events';

export const useStoreValue = (
	path: PathSegment[] | undefined
): [
	DataValue,
	(value: DataValue, overridePath?: PathSegment[]) => void,
	Store
] => {
	const store = useContext(StoreContext);
	const initialValue = path && store.getValue(path);
	const [value, setValue] = useState<DataValue>(initialValue);

	useEffect(() => {
		if (path) {
			setValue(store.getValue(path));
			const unsubscribe = store.subscribe(path, value => {
				setValue(value);
			});
			return unsubscribe;
		}
	}, []);

	const setStoreValue = useCallback(
		(value, overridePath) => {
			if (path) {
				store.setValue(overridePath || path, value);
			}
		},
		[store]
	);

	return [value, setStoreValue, store];
};

export const useStoreShallowValue = (
	path: PathSegment[] | undefined,
	level = 0
): DataValue => {
	const store = useContext(StoreContext);
	const initialValue = path && store.getValue(path);
	const [value, setValue] = useState<DataValue>(initialValue);

	useEffect(() => {
		if (path) {
			const unsubscribe = store.subscribe(path, (value, data, changedPath) => {
				const allowedChangedPathLength = path.length + level;
				if (
					changedPath.length <=
					allowedChangedPathLength /* TODO: && equals(path, changedPath.slice(0, allowedChangedPathLength))*/
				) {
					setValue(value);
				}
			});
			return unsubscribe;
		}
	}, [store]);

	return value;
};

const getDataPath = (
	pathString: string | undefined,
	parentDataPath: PathSegment[]
) => {
	if (!pathString || !isPath(pathString)) {
		return;
	}

	return getAbsolutePathArray(pathString, parentDataPath);
};

export const useStoreDataPath = (
	pathString: string | undefined,
	parentDataPath: PathSegment[],
	context: EventContext
): StoreHookValue => {
	const isPathString = pathString && isPath(pathString);
	const [pathStringValue, ...transforms] =
		pathString && isPath(pathString) ? pathString.split('|') : [pathString];

	const dataPath = getDataPath(pathStringValue, parentDataPath);

	const [storeValue, setStoreValue] = useStoreValue(dataPath);
	const transformedValue = usePipeTransformation(
		transforms.join('|'),
		storeValue,
		parentDataPath,
		context
	);

	const value = isPathString
		? transformedValue
		: pathString && unescapePath(pathString);

	return [value, isPathString ? setStoreValue : noop, dataPath || []];
};

export const useInputStoreDataPath = (
	pathString: string | undefined,
	parentDataPath: PathSegment[]
): StoreHookValue => {
	const isPathString = pathString && isPath(pathString);
	const dataPath = getDataPath(pathString, parentDataPath);
	const [storeValue, setStoreValue] = useStoreValue(dataPath);

	return [storeValue, isPathString ? setStoreValue : noop, dataPath || []];
};
