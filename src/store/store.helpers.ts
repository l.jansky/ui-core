import { DataValue } from './store.types';
import {
	PathSegment,
	getAbsolutePathArray,
	isPath
} from '../common/path/path.helpers';
import { path } from 'ramda';

// TODO: this function should probably be used in store.hook

export const getDataValue = (
	pathString: string | undefined,
	parentDataPath: PathSegment[],
	data: any
): DataValue => {
	if (typeof pathString === 'undefined') {
		return;
	}

	if (!isPath(pathString)) {
		if (pathString.startsWith('//')) {
			return pathString.substr(1);
		} else {
			return pathString;
		}
	}

	const dataPath = getAbsolutePathArray(pathString, parentDataPath);
	return path(dataPath, data);
};
