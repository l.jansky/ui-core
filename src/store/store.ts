import { lensPath, set, dissocPath, view, equals } from 'ramda';
import { Data, DataValue } from './store.types';
import { PathSegment } from '../common/path/path.helpers';

const callbacks = Symbol('callbacks');

type SubscriptionCallback = (
	value: DataValue,
	data: Data,
	path: PathSegment[]
) => void;

type SubscriptionPathItem = {
	[key: string]: SubscriptionPathItem;
	[callbacks]: SubscriptionCallback[];
};

export type Store = {
	subscribe: (
		path: PathSegment[],
		callback: SubscriptionCallback
	) => () => void;
	setValue: (path: PathSegment[], value: DataValue) => void;
	getValue: (path: PathSegment[]) => DataValue;
	getData: () => Data;
};

const recursiveEmit = (
	subscriptions: SubscriptionPathItem,
	data: DataValue,
	absoluteData: Data,
	changedPath: PathSegment[]
) => {
	for (const i in subscriptions) {
		subscriptions[i][callbacks].forEach(callback =>
			callback(
				data && (data as any)[i] ? (data as any)[i] : undefined,
				absoluteData,
				changedPath
			)
		);
		recursiveEmit(
			subscriptions[i],
			data && (data as any)[i],
			absoluteData,
			changedPath
		);
	}
};

const unsubscribe = (
	callback: SubscriptionCallback,
	subscriptions: SubscriptionPathItem
) => {
	subscriptions[callbacks] = subscriptions[callbacks].filter(
		cb => cb !== callback
	);
	for (const key in subscriptions) {
		unsubscribe(callback, subscriptions[key]);
	}
};

export const getStore = (initialData: Data = {}): Store => {
	let data: Data = initialData;
	const subscriptions: SubscriptionPathItem = {
		[callbacks]: []
	};

	return {
		subscribe: (path: PathSegment[], callback: SubscriptionCallback) => {
			let actualSubscriptionPathItem = subscriptions;
			for (const segment of path) {
				if (!actualSubscriptionPathItem[segment]) {
					actualSubscriptionPathItem[segment.toString()] = {
						[callbacks]: []
					};
				}
				actualSubscriptionPathItem =
					actualSubscriptionPathItem[segment.toString()];
			}
			actualSubscriptionPathItem[callbacks].push(callback);

			return () => {
				unsubscribe(callback, subscriptions);
			};
		},
		setValue: (path: PathSegment[], value: DataValue) => {
			const lensDataPath = lensPath(path);
			const currentValue = view(lensDataPath, data);
			if (equals(currentValue, value)) {
				// TODO: write test for this
				return;
			}

			if (typeof value === 'undefined') {
				data = dissocPath(path, data);
			} else {
				data = set(lensDataPath, value, data);
			}

			for (let i = path.length; i >= 0; i--) {
				const p = lensPath(path.slice(0, i));
				const pathSubscriptions = view<
					SubscriptionPathItem,
					SubscriptionPathItem
				>(p, subscriptions);
				pathSubscriptions &&
					pathSubscriptions[callbacks].forEach(callback =>
						callback(view(p, data), data, path)
					);
			}

			const actualSubscriptionPathItem = view<
				SubscriptionPathItem,
				SubscriptionPathItem
			>(lensDataPath, subscriptions);
			recursiveEmit(
				actualSubscriptionPathItem,
				view(lensDataPath, data),
				data,
				path
			);
		},
		getValue: (path: PathSegment[]): DataValue => {
			const lensDataPath = lensPath(path);
			return view(lensDataPath, data);
		},
		getData: (): Data => {
			return data;
		}
	};
};
