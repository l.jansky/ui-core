import React from 'react';
import { renderHook, act } from '@testing-library/react-hooks';
import { useStoreValue, useStoreDataPath } from './store.hook';
import { StoreContextProvider } from './store.context';
import { getStore, Store } from './store';
import { PipeContextProvider } from '../pipe';
import { DEFAULT_PIPE_FUNCTIONS } from '../pipe/functions';

const getWrapper = (store: Store): React.FC => {
	return ({ children }) => (
		<PipeContextProvider functions={DEFAULT_PIPE_FUNCTIONS}>
			<StoreContextProvider store={store}>{children}</StoreContextProvider>
		</PipeContextProvider>
	);
};

describe('useStoreValue hook', () => {
	it('should get initial value from store', () => {
		const store = getStore({ a: 1 });
		const {
			result: {
				current: [value]
			}
		} = renderHook(() => useStoreValue(['a']), {
			wrapper: getWrapper(store)
		});
		expect(value).toEqual(1);
	});

	it('should set value in store', () => {
		const { result } = renderHook(() => useStoreValue(['a']), {
			wrapper: getWrapper(getStore())
		});
		act(() => {
			result.current[1](1);
		});
		expect(result.current[0]).toEqual(1);
	});
});

describe('useStoreDataPath hook', () => {
	it('should get path as data if path is not path but static value', () => {
		const store = getStore();
		const { result } = renderHook(
			() => useStoreDataPath('static value', [], []),
			{
				wrapper: getWrapper(store)
			}
		);

		act(() => {
			const [, setValue] = result.current;
			setValue('ignored');
		});

		const [value] = result.current;
		expect(value).toBe('static value');
		expect(store.getData()).toEqual({});
	});

	it('should set and get data', async () => {
		const store = getStore();
		const { result } = renderHook(
			() => useStoreDataPath('/test/subValue', [], []),
			{
				wrapper: getWrapper(store)
			}
		);
		const value = 'changed-sub-value';

		await act(async () => {
			const [, setValue] = result.current;
			setValue(value);
		});

		const [currentValue] = result.current;
		expect(store.getData()).toEqual({
			test: {
				subValue: value
			}
		});

		expect(currentValue).toBe(value);
	});

	it('should get transformed data from store', async () => {
		const testValue = 'test value';
		const store = getStore();
		const { result } = renderHook(
			() => useStoreDataPath('/testValue|uppercase', [], []),
			{
				wrapper: getWrapper(store)
			}
		);

		await act(async () => {
			const [, setValue] = result.current;
			setValue(testValue);
		});

		const [value] = result.current;
		expect(value).toBe(testValue.toUpperCase());
	});
});
