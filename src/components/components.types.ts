import React from 'react';
import { PathSegment } from '../common/path/path.helpers';

export type PrejtContext = string[];

export type PrejtComponentProps<T> = T & {
	childConfigs?: ComponentConfig[];
	parentDataPath: PathSegment[];
	eventContext: PrejtContext;
};

export type PrejtComponent<T> = React.FC<PrejtComponentProps<T>>;

export type ComponentConfig<T = Record<string, string>> = {
	name: string;
	attributes: T;
	childConfigs?: ComponentConfig[];
};

export type ComponentsDefinitions = {
	[name: string]: React.FunctionComponent<any>;
};

export type ComponentsContextProviderProps = {
	componentsDefinitions: ComponentsDefinitions;
};

export type getComponent = (
	parentPathArray: PathSegment[],
	eventContext: PrejtContext
) => (config: ComponentConfig, index: number) => React.ReactNode;
