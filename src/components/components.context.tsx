import React from 'react';
import {
	getComponent,
	ComponentsContextProviderProps,
	ComponentConfig,
	PrejtContext
} from './components.types';
import { DEFAULT_COMPONENTS } from './default-components';
import { PathSegment } from '../common/path/path.helpers';

export const ComponentsContext = React.createContext<getComponent>(() => () =>
	null
);

export const ComponentsContextProvider: React.FunctionComponent<ComponentsContextProviderProps> = ({
	componentsDefinitions,
	children
}) => {
	const getComponent: getComponent = (
		parentDataPath: PathSegment[],
		eventContext: PrejtContext
	) => (
		{ name, attributes, childConfigs }: ComponentConfig<unknown>,
		index
	) => {
		const allComponentsDefinitions = {
			...DEFAULT_COMPONENTS,
			...componentsDefinitions
		};

		const Component = allComponentsDefinitions[name];
		if (Component) {
			return (
				<Component
					key={index}
					{...attributes}
					childConfigs={childConfigs}
					parentDataPath={parentDataPath}
					eventContext={eventContext}
				/>
			);
		} else {
			throw `Prejt component ${name} not found.`;
		}
	};

	return (
		<ComponentsContext.Provider value={getComponent}>
			{children}
		</ComponentsContext.Provider>
	);
};
