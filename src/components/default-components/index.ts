import { FetcherComponent } from './fetcher/fetcher';
import { StringComponent } from './string/string';
import { ValueComponent } from './value/value';
import { SubscribeComponent } from './subscribe/subscribe';
import { Fragment } from './fragment/fragment';
import { ComponentsDefinitions } from '../components.types';
import { withEnabled } from '../hoc/with-enabled/with-enabled';

const components: ComponentsDefinitions = {
	fetcher: FetcherComponent,
	string: StringComponent,
	value: ValueComponent,
	subscribe: SubscribeComponent,
	fragment: Fragment
};

export const DEFAULT_COMPONENTS = Object.assign(
	{},
	...Object.entries(components).map(([k, component]) => ({
		[k]: withEnabled(component)
	}))
);
