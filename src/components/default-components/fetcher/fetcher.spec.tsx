import React from 'react';
import { render, act } from '@testing-library/react';
import { FetcherComponent } from './fetcher';
import { getAppTree } from '../../../utils/test/app-tree';
import { eventEmitterFactory } from '../../../events';
import { PipeFunctions, getPipeIO } from '../../../pipe';
import fetchMock from 'fetch-mock';
import '@testing-library/jest-dom/extend-expect';
import { flushPromises } from '../../../utils/test';

describe('Fetcher component', () => {
	afterEach(() => {
		fetchMock.restore();
	});

	beforeEach(() => {
		jest.clearAllMocks();
	});

	it('should fetch data on event', async () => {
		const eventEmitter = eventEmitterFactory()();
		const pipeFunctions: PipeFunctions = {
			test: jest.fn()
		};
		const responseBody = { ok: 1 };
		fetchMock.mock('http://example.com', { body: responseBody });
		const [tree] = getAppTree(
			<FetcherComponent
				fetch="http://example.com"
				listenTo="event1"
				onData="test"
				parentDataPath={[]}
				eventContext={[]}
			/>,
			{
				eventEmitter,
				pipeFunctions
			}
		);

		render(tree);

		await act(async () => {
			eventEmitter.emit('event1', [], {});
			await flushPromises();
		});

		expect(pipeFunctions.test).toHaveBeenCalledTimes(1);
		expect(pipeFunctions.test).toHaveBeenCalledWith(
			expect.objectContaining({
				input: getPipeIO(responseBody)
			})
		);
	});

	it('should fetch data on mount and after param update', async () => {
		const pipeFunctions: PipeFunctions = {
			test: jest.fn()
		};
		const response1Body = { responseId: 1 };
		const response2Body = { responseId: 2 };
		fetchMock.mock('http://example.com/1', { body: response1Body });
		fetchMock.mock('http://example.com/2', { body: response2Body });

		const initialStoreData = {
			params: {
				id: 1
			}
		};

		const [tree, getCurrentStore] = getAppTree(
			<FetcherComponent
				fetch="http://example.com/:id"
				onData="test"
				params="/params"
				parentDataPath={[]}
				eventContext={[]}
				autofetch={'true'}
			/>,
			{
				initialStoreData,
				pipeFunctions
			}
		);

		await act(async () => {
			render(tree);
			await flushPromises();
		});

		expect(pipeFunctions.test).toHaveBeenCalledTimes(1);
		expect(pipeFunctions.test).toHaveBeenLastCalledWith(
			expect.objectContaining({
				input: getPipeIO(response1Body)
			})
		);

		await act(async () => {
			getCurrentStore().setValue(['params', 'id'], 2);
			await flushPromises();
		});

		expect(pipeFunctions.test).toHaveBeenCalledTimes(2);
		expect(pipeFunctions.test).toHaveBeenLastCalledWith(
			expect.objectContaining({
				input: getPipeIO(response2Body)
			})
		);
	});

	it('should post data from event', async () => {
		const eventEmitter = eventEmitterFactory()();
		const pipeFunctions: PipeFunctions = {
			test: jest.fn()
		};
		const postData = { post: 1 };
		const responseBody = { ok: 1 };
		fetchMock.mock(
			{
				url: 'http://example.com',
				method: 'POST',
				body: postData
			},
			{ body: responseBody }
		);

		const [tree] = getAppTree(
			<FetcherComponent
				fetch="http://example.com"
				listenTo="event"
				onData="test"
				parentDataPath={[]}
				eventContext={[]}
				method="POST"
			/>,
			{
				eventEmitter,
				pipeFunctions
			}
		);

		render(tree);

		await act(async () => {
			eventEmitter.emit('event', [], postData);
			await flushPromises();
		});

		expect(pipeFunctions.test).toHaveBeenCalledTimes(1);
		expect(pipeFunctions.test).toHaveBeenCalledWith(
			expect.objectContaining({
				input: getPipeIO(responseBody)
			})
		);
	});

	it.todo('should skip some requests if debounce is set');
});
