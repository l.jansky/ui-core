import React from 'react';
import { PrejtComponent } from '../../components.types';
import {
	useStoreDataPath,
	useFetch,
	DataValue,
	FetchMethod
} from '../../../store';
import { debounce } from '../../../utils/debounce/debounce';
import { useEvents } from '../../../events';
import { useInvokePipe, getPipeIO } from '../../../pipe';

type FetcherComponentProps = {
	fetch: string;
	method?: FetchMethod;
	onData?: string;
	onError?: string;
	debounce?: string;
	params?: string;
	query?: string;
	listenTo?: string;
	autofetch?: string;
};

export const FetcherComponent: PrejtComponent<FetcherComponentProps> = ({
	fetch,
	onData = '',
	onError = '',
	method = 'GET',
	debounce: debounceParam,
	parentDataPath,
	params,
	query,
	listenTo,
	autofetch,
	eventContext
}) => {
	const [fetchUrl] = useStoreDataPath(fetch, parentDataPath, eventContext);
	const [paramsValue] = useStoreDataPath(params, parentDataPath, eventContext);
	const [queryValue] = useStoreDataPath(query, parentDataPath, eventContext);
	const [debounceValue] = useStoreDataPath(
		debounceParam,
		parentDataPath,
		eventContext
	);
	const invokePipe = useInvokePipe(onData, parentDataPath, eventContext);
	const invokeErrorPipe = useInvokePipe(onError, parentDataPath, eventContext);
	// TODO: should be also onError and useFetch invoke onErrorPipe as second parameter
	const { doFetch } = useFetch<DataValue>(
		data => invokePipe(getPipeIO(data)),
		error => invokeErrorPipe(getPipeIO(error))
	);

	const doDebouncedFetch = React.useCallback(
		debounce(doFetch, (debounceValue as any) || 0),
		[debounceValue]
	);

	const fetchData = (body?: DataValue) => {
		const isQueryLoaded = !query || !!queryValue;
		const isParamsLoaded = !params || !!paramsValue;
		if (isQueryLoaded && isParamsLoaded) {
			doDebouncedFetch(fetchUrl as string, {
				params: paramsValue as any,
				query: queryValue as any,
				method,
				body
			});
		}
	};

	useEvents(
		(listenTo && listenTo.split(',')) || [],
		eventContext,
		eventData => {
			fetchData(eventData);
		}
	);

	React.useEffect(() => {
		if (autofetch === 'true') {
			fetchData();
		}
	}, [fetchUrl, paramsValue, queryValue, query, params]);

	return null;
};
