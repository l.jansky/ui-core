import React from 'react';
import { render, act } from '@testing-library/react';
import { StringComponent } from './string';
import { getAppTree } from '../../../utils/test/app-tree';
import '@testing-library/jest-dom/extend-expect';

describe('String component', () => {
	const initialValue = 'initial value';
	const updatedValue = 'updated value';
	const initialStoreData = { test: initialValue };
	it('should render string component with correct value without transformation', async () => {
		const [tree, getCurrentStore] = getAppTree(
			<StringComponent value="/test" parentDataPath={[]} eventContext={[]} />,
			{ initialStoreData }
		);
		const { findByText } = render(tree);
		await findByText(initialValue);

		act(() => {
			getCurrentStore().setValue(['test'], updatedValue);
		});

		await findByText(updatedValue);
	});

	it('should render string component with correct transformed value', async () => {
		const [tree, getCurrentStore] = getAppTree(
			<StringComponent
				value="/test|delay(0)|uppercase"
				parentDataPath={[]}
				eventContext={[]}
			/>,
			{ initialStoreData }
		);
		const { findByText } = render(tree);
		await findByText(initialValue.toUpperCase());

		act(() => {
			getCurrentStore().setValue(['test'], updatedValue);
		});

		await findByText(updatedValue.toUpperCase());
	});
});
