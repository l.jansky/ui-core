import React from 'react';
import { PrejtComponent, useStoreDataPath } from '../../../index';

type StringComponentProps = {
	value: string;
};

export const StringComponent: PrejtComponent<StringComponentProps> = ({
	value,
	parentDataPath,
	eventContext
}) => {
	const [dataValue] = useStoreDataPath(value, parentDataPath, eventContext);
	return <>{dataValue}</>;
};
