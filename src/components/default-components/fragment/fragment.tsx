import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	getAbsolutePathArray,
	Container
} from '../../../index';

type FragmentComponentProps = {
	data?: string;
	module?: string;
};

export const Fragment: PrejtComponent<FragmentComponentProps> = ({
	childConfigs = [],
	data = './',
	module,
	parentDataPath,
	eventContext
}) => {
	const dataPath = getAbsolutePathArray(data, parentDataPath);
	const [moduleValue] = useStoreDataPath(module, parentDataPath, eventContext);
	return (
		<Container
			childConfigs={childConfigs}
			dataPath={dataPath}
			module={moduleValue as string}
			eventContext={eventContext}
		/>
	);
};
