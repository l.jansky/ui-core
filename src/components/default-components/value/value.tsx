import React from 'react';
import {
	PrejtComponent,
	useStoreDataPath,
	useInvokePipe,
	getPipeIO
} from '../../../index';

type ValueComponentProps = {
	value: string;
	onChange?: string;
	filter?: string;
};

export const ValueComponent: PrejtComponent<ValueComponentProps> = ({
	value,
	parentDataPath,
	onChange = '',
	eventContext,
	filter = ''
}) => {
	const [dataValue] = useStoreDataPath(value, parentDataPath, eventContext);
	const invokeOnChangePipe = useInvokePipe(
		onChange,
		parentDataPath,
		eventContext
	);
	const invokeFilterPipe = useInvokePipe(filter, parentDataPath, eventContext);

	React.useEffect(() => {
		if (filter) {
			invokeFilterPipe(getPipeIO(dataValue)).then(({ value }) => {
				if (value) {
					invokeOnChangePipe(getPipeIO(dataValue));
				}
			});
		} else {
			invokeOnChangePipe(getPipeIO(dataValue));
		}
	}, [dataValue]);

	return null;
};
