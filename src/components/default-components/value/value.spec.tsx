import React from 'react';
import { render, act } from '@testing-library/react';
import { ValueComponent } from './value';
import { getAppTree } from '../../../utils/test/app-tree';

describe('Value component', () => {
	it('should set value to store after rendered', async () => {
		const [tree, getCurrentStore] = getAppTree(
			<ValueComponent
				value="test"
				onChange="uppercase|store(/test)"
				parentDataPath={[]}
				eventContext={[]}
			/>
		);

		await act(async () => {
			render(tree);
		});

		expect(getCurrentStore().getData()).toEqual({ test: 'TEST' });
	});
});
