import { PrejtComponent } from '../../components.types';
import { useEvents } from '../../../events';
import { useInvokePipe, getPipeIO } from '../../../pipe';

type SubscribeComponentProps = {
	listenTo: string;
	onEvent: string;
	filter?: string;
};

export const SubscribeComponent: PrejtComponent<SubscribeComponentProps> = ({
	listenTo,
	onEvent,
	parentDataPath,
	eventContext,
	filter = ''
}) => {
	const invokeOnEventPipe = useInvokePipe(
		onEvent,
		parentDataPath,
		eventContext
	);
	const invokeFilterPipe = useInvokePipe(filter, parentDataPath, eventContext);

	useEvents(listenTo.split(','), eventContext, eventData => {
		if (filter) {
			invokeFilterPipe(getPipeIO(eventData)).then(({ value }) => {
				if (value) {
					invokeOnEventPipe(getPipeIO(eventData));
				}
			});
		} else {
			invokeOnEventPipe(getPipeIO(eventData));
		}
	});

	return null;
};
