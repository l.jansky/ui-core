import React from 'react';
import { render, act } from '@testing-library/react';
import { SubscribeComponent } from './subscribe';
import { eventEmitterFactory } from '../../../events';
import { getAppTree, flushPromises } from '../../../utils/test';
import {
	PipeFunctions,
	DEFAULT_PIPE_FUNCTIONS,
	getPipeIO
} from '../../../pipe';

describe('Subscribe component', () => {
	beforeEach(() => {
		jest.clearAllMocks();
	});

	it('should call action after event is emited', async () => {
		const eventEmitter = eventEmitterFactory()();
		const pipeFunctions: PipeFunctions = {
			test: jest.fn()
		};
		const [tree] = getAppTree(
			<SubscribeComponent
				listenTo={'event1,event3'}
				onEvent={'test'}
				parentDataPath={[]}
				eventContext={['root']}
			/>,
			{
				eventEmitter,
				pipeFunctions
			}
		);
		render(tree);

		act(() => {
			eventEmitter.emit('event1', ['root'], 1);
		});

		expect(pipeFunctions.test).toHaveBeenCalledTimes(1);
		expect(pipeFunctions.test).toHaveBeenLastCalledWith(
			expect.objectContaining({
				input: getPipeIO(1)
			})
		);

		act(() => {
			eventEmitter.emit('event1', [], 2);
		});

		act(() => {
			eventEmitter.emit('event2', ['root'], 3);
		});

		expect(pipeFunctions.test).toHaveBeenCalledTimes(1);

		act(() => {
			eventEmitter.emit('event3', ['root', 'test'], 4);
		});

		expect(pipeFunctions.test).toHaveBeenCalledTimes(2);
		expect(pipeFunctions.test).toHaveBeenLastCalledWith(
			expect.objectContaining({
				input: getPipeIO(4)
			})
		);
	});

	it('should filter some action calls based on event input data', async () => {
		const eventEmitter = eventEmitterFactory()();
		const pipeFunctions: PipeFunctions = {
			...DEFAULT_PIPE_FUNCTIONS,
			test: jest.fn()
		};
		const [tree] = getAppTree(
			<SubscribeComponent
				listenTo={'event'}
				onEvent={'test'}
				parentDataPath={[]}
				eventContext={['root']}
				filter="eq(b)"
			/>,
			{
				eventEmitter,
				pipeFunctions
			}
		);
		render(tree);

		act(() => {
			eventEmitter.emit('event', ['root'], 'a');
		});

		expect(pipeFunctions.test).toHaveBeenCalledTimes(0);

		act(() => {
			eventEmitter.emit('event', ['root'], 'b');
		});

		await flushPromises();

		expect(pipeFunctions.test).toHaveBeenCalledTimes(1);
	});
});
