import React from 'react';
import { render, act } from '@testing-library/react';
import fetchMock from 'fetch-mock';
import { ModuleOpener, MODULE_OPENER_LIST_DATA_PATH } from './module-opener';
import { ModuleOpenerItem } from './module-opener.types';
import { getAppTree } from '../../../utils/test/app-tree';
import '@testing-library/jest-dom/extend-expect';
import { Store } from '../../../store';
import { ComponentConfig } from '../../components.types';

const appendModuleOpenerItem = (store: Store, item: ModuleOpenerItem) => {
	const currentList = store.getValue([MODULE_OPENER_LIST_DATA_PATH]) as any[];
	store.setValue(
		[MODULE_OPENER_LIST_DATA_PATH],
		[...(currentList || []), item]
	);
};

describe('ModuleOpener component', () => {
	afterEach(() => {
		fetchMock.restore();
	});

	it('should show component', async () => {
		const testString = 'test string';
		const stringModuleResponse: ComponentConfig<unknown> = {
			name: 'module',
			attributes: {},
			childConfigs: [
				{
					name: 'string',
					attributes: {
						value: './test'
					}
				}
			]
		};
		fetchMock.mock('http://localhost/string-module', {
			body: stringModuleResponse
		});
		const [tree, getCurrentStore] = getAppTree(<ModuleOpener />);

		const { queryByText, findByText } = render(tree);
		const testStringComponent = queryByText(testString);
		expect(testStringComponent).not.toBeInTheDocument();

		act(() => {
			appendModuleOpenerItem(getCurrentStore(), {
				module: 'http://localhost/string-module',
				data: {
					test: testString
				},
				context: ['test']
			});
		});

		await findByText(testString);
	});
});
