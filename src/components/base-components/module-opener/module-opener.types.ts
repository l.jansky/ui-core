import { EventContext } from '../../../events';
import { DataValue } from '../../../store';
import { ComponentConfig } from '../../components.types';

export type ModuleOpenerProps = null;

export type ModuleOpenerItem = {
	module?: string;
	childConfigs?: ComponentConfig[];
	context: EventContext;
	data: DataValue;
};
