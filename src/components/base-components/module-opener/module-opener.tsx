import React from 'react';
import { ModuleOpenerItem } from './module-opener.types';
import { useStoreShallowValue } from '../../../store';
import { Container } from '../container/container';

export const MODULE_OPENER_LIST_DATA_PATH = '__moduleOpener';

export const ModuleOpener: React.FC = () => {
	const shallowModuleList = useStoreShallowValue(
		[MODULE_OPENER_LIST_DATA_PATH],
		1
	);
	const moduleList = shallowModuleList || [];
	if (!Array.isArray(moduleList)) {
		throw new Error('Module opener list is not array');
	}

	return (
		<>
			{(moduleList as ModuleOpenerItem[]).map((item, index) =>
				item && item.context ? (
					<Container
						key={item.context.join() + index}
						module={item.module}
						childConfigs={item.childConfigs || []}
						eventContext={item.context}
						dataPath={[MODULE_OPENER_LIST_DATA_PATH, index, 'data']}
					/>
				) : null
			)}
		</>
	);
};
