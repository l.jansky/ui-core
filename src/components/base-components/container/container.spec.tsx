import React from 'react';
import { render } from '@testing-library/react';
import fetchMock from 'fetch-mock';
import { Container } from './container';
import { getAppTree } from '../../../utils/test/app-tree';
import '@testing-library/jest-dom/extend-expect';
import { ComponentConfig } from '../../components.types';

describe('Container component', () => {
	afterEach(() => {
		fetchMock.restore();
	});

	it('should render components in childConfigs', async () => {
		const testString = 'test string';

		const childConfigs: ComponentConfig<any>[] = [
			{
				name: 'string',
				attributes: {
					value: testString
				}
			}
		];

		const [tree] = getAppTree(
			<Container childConfigs={childConfigs} dataPath={[]} eventContext={[]} />
		);

		const { findByText } = render(tree);

		await findByText(testString);
	});

	it('should render components loaded from url in module attribute', async () => {
		const testString = 'test string';
		const stringModuleResponse: ComponentConfig<unknown> = {
			name: 'module',
			attributes: {},
			childConfigs: [
				{
					name: 'string',
					attributes: {
						value: testString
					}
				}
			]
		};
		fetchMock.mock('http://localhost/string-module', {
			body: stringModuleResponse
		});
		const [tree] = getAppTree(
			<Container
				childConfigs={[]}
				module={'http://localhost/string-module'}
				dataPath={[]}
				eventContext={[]}
			/>
		);

		const { findByText } = render(tree);

		await findByText(testString);
	});
});
