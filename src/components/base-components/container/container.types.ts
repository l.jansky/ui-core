import { ComponentConfig, PrejtContext } from '../../components.types';
import { PathSegment } from '../../../common/path/path.helpers';

export type ContainerProps = {
	childConfigs: ComponentConfig[];
	dataPath: PathSegment[];
	eventContext: PrejtContext;
	module?: string;
};
