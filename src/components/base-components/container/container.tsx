import React from 'react';
import { uuid } from 'uuidv4';
import { ContainerProps } from './container.types';
import { ComponentsContext } from '../../components.context';
import { ComponentConfig } from '../../components.types';
import { useFetch } from '../../../store';

export const Container: React.FC<ContainerProps> = ({
	childConfigs,
	dataPath = [],
	module,
	eventContext
}) => {
	const getComponent = React.useContext(ComponentsContext);
	const [contextUuid] = React.useState(uuid());
	const containerContext = React.useMemo(() => [...eventContext, contextUuid], [
		eventContext,
		contextUuid
	]);
	const [fetchedModule, setFetchedModule] = React.useState<
		ComponentConfig | undefined
	>();
	const { doFetch, isLoading } = useFetch<ComponentConfig>(data =>
		setFetchedModule(data)
	);

	React.useEffect(() => {
		if (module) {
			doFetch(module);
		}
	}, [module]);

	const renderConfigs =
		(fetchedModule && fetchedModule.childConfigs) || childConfigs;

	return (
		<>
			{!isLoading &&
				renderConfigs &&
				renderConfigs.map(getComponent(dataPath, containerContext))}
		</>
	);
};
