import {
	ComponentConfig,
	PathSegment,
	getAbsolutePathArray,
	useStoreShallowValue
} from '../../..';

export const useItems = (
	childConfigs: ComponentConfig<any>[],
	items: string | undefined,
	parentDataPath: PathSegment[]
) => {
	const dataPath = getAbsolutePathArray(items || '', parentDataPath);
	const itemsData = useStoreShallowValue(dataPath);

	if (items && childConfigs.length > 0) {
		const {
			attributes,
			childConfigs: itemChildConfigs,
			name
		} = childConfigs[0];
		return ((itemsData as any[]) || []).map(({}, key) => ({
			name,
			key,
			attributes,
			dataPath: getAbsolutePathArray(`./${key}`, dataPath),
			childConfigs: itemChildConfigs
		}));
	} else {
		return childConfigs.map(
			({ attributes, childConfigs: itemChildConfigs, name }, key) => ({
				name,
				key,
				attributes,
				dataPath: getAbsolutePathArray(`./${key}`, dataPath),
				childConfigs: itemChildConfigs
			})
		);
	}
};
