import React from 'react';
import { PathSegment } from '../../../common';
import { useItems } from './items.hook';
import { PrejtContext, ComponentConfig } from '../../components.types';
import { ComponentsContext } from '../../components.context';

export type ItemsProps = {
	childConfigs: ComponentConfig<unknown>[];
	dataPath: PathSegment[];
	eventContext: PrejtContext;
	items?: string;
};

export const Items: React.FC<ItemsProps> = ({
	childConfigs,
	dataPath = [],
	items,
	eventContext
}) => {
	const getComponent = React.useContext(ComponentsContext);
	const itemConfigs = useItems(childConfigs, items, dataPath);
	return (
		<>
			{itemConfigs.map(({ dataPath, key, ...itemConfig }) =>
				getComponent(dataPath, eventContext)(itemConfig, key)
			)}
		</>
	);
};
