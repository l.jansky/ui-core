import React from 'react';
import { EnabledProps } from './with-enabled.types';
import {
	PrejtComponentProps,
	useStoreDataPath,
	PrejtComponent
} from '../../../index';

export const withEnabled = (
	Component: PrejtComponent<any>
): React.FC<EnabledProps & PrejtComponentProps<any>> => ({
	enabled,
	parentDataPath,
	context,
	...props
}) => {
	const [enabledData] = useStoreDataPath(enabled, parentDataPath, context);
	return typeof enabled === 'undefined' || enabledData ? (
		<Component {...props} parentDataPath={parentDataPath} />
	) : null;
};
