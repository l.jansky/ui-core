const TEST_REGEX = '(/__tests__/.*|(\\.|/)(test|spec))\\.(tsx?|ts?)$';

module.exports = {
	setupFiles: ['<rootDir>/jest.setup.js'],
	setupFilesAfterEnv: [
		'@testing-library/jest-dom/extend-expect'
	],
	testEnvironment: 'jsdom',
	testRegex: TEST_REGEX,
	transform: {
		'^.+\\.tsx?$': 'ts-jest',
		'\\.jsx?$': 'babel-jest',
		'\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
			'<rootDir>/jest.fileTransformer.js'
	},
	snapshotSerializers: [],
	testPathIgnorePatterns: ['<rootDir>/dist/', '<rootDir>/node_modules/'],
	moduleNameMapper: {
		'\\.(css)$': 'identity-obj-proxy',
		'\\.(gif|ttf|eot|svg)$': '<rootDir>/__mocks__/fileMock.js'
	},
	moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json'],
	collectCoverageFrom: ['**/*.{ts,tsx}', '!**/*.d.ts', '!**/*.styled.ts'],
	globals: {
		'ts-jest': {
			babelConfig: true,
			disableSourceMapSupport: true // Fixes issue with collectCoverage. Ref: https://github.com/kulshekhar/ts-jest/issues/614
		}
	},
	coveragePathIgnorePatterns: [
		'dangerfile.ts',
		'/node_modules/',
		'.config.',
		'/config/',
		'/__generated__/',
		'/queries/',
		'/mutations/'
	]
};
